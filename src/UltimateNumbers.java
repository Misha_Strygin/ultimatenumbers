import java.util.LinkedHashSet;
import java.util.Scanner;

public class UltimateNumbers {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Кол-во элементов: ");
        int count = scanner.nextInt();

        //массив повторяющихся значений
        String[] array = new String[count];

        //вводим с клавиатуры данные
        for (int i = 0; i < count; i++) {
            array[i] = scanner.next();
        }

        /*
         * Set - интерфейс "множество" позволяет хранить ТОЛЬКО уникальыне значения
         */
        LinkedHashSet<String> sequence = new LinkedHashSet<String>();

        for (int i = 0; i < count; i++) {
            //добавляем элемент в множество, если его там ещё нет
            sequence.add(array[i]);
        }

        //выводим множество
        System.out.println(sequence);
    }
}
